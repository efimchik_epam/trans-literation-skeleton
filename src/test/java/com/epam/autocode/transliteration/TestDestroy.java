package com.epam.autocode.transliteration;

import org.junit.jupiter.api.*;

import java.util.Arrays;

@DisplayName("Destroy test")
public class TestDestroy {
    static StreamMutator streamMutator;

    @BeforeAll
    static void initMutator() {
        streamMutator = new StreamMutator(Arrays.stream(new String[]{}));
    }

    @BeforeEach
    void disposeMutator(){
        streamMutator.destroy();
    }

    @Test
    @DisplayName("Input stream close() test")
    void inputStreamCloseTest(){
        streamMutator.inputStream = Arrays.stream(new String[]{});
        streamMutator.destroy();
        Assertions.assertThrows(IllegalStateException.class, () -> streamMutator.inputStream.count());
    }

    @Test
    @DisplayName("Output stream close() test")
    void outputStreamCloseTest(){
        streamMutator.setInputStream(Arrays.stream(new String[]{}));
        streamMutator.destroy();
        Assertions.assertThrows(IllegalStateException.class, () -> streamMutator.outputStream.count());
    }
}
