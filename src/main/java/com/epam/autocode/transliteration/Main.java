package com.epam.autocode.transliteration;

import java.util.Arrays;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Stream<String> phrasesStream = Arrays.stream(new String[]{
                "кот в кедах",
                "Смотрю мультик \"Тачки 2\"",
                "КОСМОНАВТ"
        });

        StreamMutator mutator = new StreamMutator(phrasesStream);

        mutator.outputStream.forEach(System.out::println);

        mutator.destroy();
    }
}
