package com.epam.autocode.transliteration;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Collections.unmodifiableMap;

public class StreamMutator {

    public Stream<String> inputStream;
    public Stream<String> outputStream;
    private static final Map<String, String> cyrillicToLatinMap = initMap();

    StreamMutator(Stream<String> InputStream){
        inputStream = InputStream;
        formulateOutputStream();
    }

    // implement string composing here, use cyrillicToLatinMap dictionary
    private void formulateOutputStream(){
        outputStream = inputStream.map(s -> {            StringBuilder result = new StringBuilder();            for(char symbol: s.toCharArray()){                if(cyrillicToLatinMap.containsKey(String.valueOf(symbol))){                    result.append(cyrillicToLatinMap.get(String.valueOf(symbol)));                } else {                    result.append(symbol);                }            }            return result.toString();        });
    }

    // correctly close streams here
    public void destroy(){
        inputStream.close();
        outputStream.close();
    }

    public void setInputStream(Stream<String> InputStream) {
        inputStream = InputStream;
        formulateOutputStream();
    }

    private static Map<String, String> initMap() {
        Map<String, String> map = new HashMap<>();
        map.put("А", "A");
        map.put("а", "a");
        map.put("Б", "6");
        map.put("б", "6");
        map.put("В", "B");
        map.put("в", "B");
        map.put("Г", "r");
        map.put("г", "r");
        map.put("Д", "g");
        map.put("д", "g");
        map.put("Е", "E");
        map.put("е", "e");
        map.put("Ё", "E");
        map.put("ё", "e");
        map.put("Ж", "}|{");
        map.put("ж", "}|{");
        map.put("З", "3");
        map.put("з", "3");
        map.put("И", "|/|");
        map.put("и", "u");
        map.put("Й", "|/|");
        map.put("й", "u");
        map.put("К", "K");
        map.put("к", "K");
        map.put("Л", "Jl");
        map.put("л", "Jl");
        map.put("М", "M");
        map.put("м", "M");
        map.put("Н", "H");
        map.put("н", "H");
        map.put("О", "O");
        map.put("о", "o");
        map.put("П", "n");
        map.put("п", "n");
        map.put("Р", "P");
        map.put("р", "p");
        map.put("С", "C");
        map.put("с", "c");
        map.put("Т", "T");
        map.put("т", "T");
        map.put("У", "y");
        map.put("у", "y");
        map.put("Ф", "(|)");
        map.put("ф", "qp");
        map.put("Х", "X");
        map.put("х", "x");
        map.put("Ц", "|/|");
        map.put("ц", "u");
        map.put("Ч", "4");
        map.put("ч", "4");
        map.put("Ш", "LLl");
        map.put("ш", "LLl");
        map.put("Щ", "LLL");
        map.put("щ", "LLL");
        map.put("Ъ", "b");
        map.put("ъ", "b");
        map.put("Ы", "bl");
        map.put("ы", "bl");
        map.put("Ь", "b");
        map.put("ь", "b");
        map.put("Э", "3");
        map.put("э", "3");
        map.put("Ю", "|-O");
        map.put("ю", "l-o");
        map.put("Я", "9l");
        map.put("я", "9l");
        return unmodifiableMap(map);
    }

}
