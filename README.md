# Ridiculous transliteration

Given not null inputstream of strings that may contains Cyrillic symbols. Implement methods in [StreamMutator](https://gitlab.com/okladnikov.bool/trans-literation-skeleton/-/blob/master/src/main/java/com/epam/autocode/transliteration/StreamMutator.java) class to compose strings for being printed in output stream.

Also implement method that correctly close streams even with nulls, ioexceptions.

***

## Examples

```
кот в кедах -> KoT B Kegax
Смотрю мультик "Тачки 2" -> CMoTp|-o MyJlTuK "Ta4Ku 2" 
КОСМОНАВТ -> KOCMOHABT
СВЕКРОВЬ -> CBEKPOBb
хохма -> xoxMa
ИВАН -> |/|BAH
```
